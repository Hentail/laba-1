import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        List<Students> students = new ArrayList<>();
        students.add(new Students("Иваной", "С.В.", "П-2-17", 4));
        students.add(new Students("Иванников", "П.А", "П-2-17", 3));
        students.add(new Students("Иванюшкин", "К.Д.", "П-2-17", 3));
        students.add(new Students("Ивашка", "Д.А.", "П-2-17", 5));
        students.add(new Students("Ивалапор", "Д.Д.", "П-2-17", 5));
        students.add(new Students("Ивапов", "Г.А.", "П-2-17", 5));
        students.add(new Students("Иватков", "В.С.", "П-2-17", 4));
        students.add(new Students("Иватцев", "Н.А.", "П-2-17", 4));
        students.add(new Students("Ивуров", "Д.А.", "П-2-17", 2));
        students.add(new Students("Ивагиев", "С.П.", "П-2-17", 1));

        Scanner scan = new Scanner(System.in);
        System.out.println("1 - Упорядоченный список/ 2 - вывод 4 и 5");
        int a = scan.nextInt();

        switch (a) {
            case 1: {
                for (int i = 0; i < students.size(); i++) {
                    for (int j = 0; j < i + 1; j++) {
                        if (students.get(j).getMark() <= students.get(i).getMark()) {
                            Students temp = students.get(j);
                            students.set(j, students.get(i));
                            students.set(i, temp);
                        }
                    }
                }
                for (int i = 0; i < students.size(); i++) {
                    System.out.println(students.get(i).toString());
                }
                break;
            }

            case 2: {
                for (int i = 0; i < students.size(); i++) {
                    for (int j = 0; j < i + 1; j++) {
                        if (students.get(j).getMark() <= students.get(i).getMark()) {
                            Students temp = students.get(j);
                            students.set(j, students.get(i));
                            students.set(i, temp);
                        }
                    }
                }
                for (int i = 0; i < students.size(); i++) {
                    if (students.get(i).getMark() == 4 || students.get(i).getMark() == 5)
                        System.out.println(students.get(i).toString());
                }
                break;
            }
        }

    }
}
