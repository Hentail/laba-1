public class Students {
    private String Surname, Inicial, Number_Putty;
    private int Mark;

    public Students (String Surname, String Inicial, String Number_Putty, int Mark){
        this.Surname = Surname;
        this.Inicial = Inicial;
        this.Number_Putty = Number_Putty;
        this.Mark = Mark;
    }

    public int getMark() {
        return Mark;
    }

    public String toString(){
        return  String.format(   "ФИО: %s %s\nНомер группы: %s\nОценка: %d\n^v^v^v^", Surname, Inicial, Number_Putty, Mark);
    }
}